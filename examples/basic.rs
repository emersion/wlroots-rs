use wlroots::allocator::Allocator;
use wlroots::backend::Backend;
use wlroots::display::Display;
use wlroots::output::Output;
use wlroots::renderer::{Box, Color, Renderer};
use wlroots::*;

#[derive(Default)]
struct TinywlServer {
    outputs: Vec<Output<Self>>,
    // TODO: these shouldn't be optional
    renderer: Option<Renderer<Self>>,
    allocator: Option<Allocator<Self>>,
}

impl backend::Handler for TinywlServer {
    fn destroy(&mut self, _: Backend<Self>) {
        println!("backend.destroy");
    }

    fn new_output(&mut self, output: Output<Self>) {
        println!("backend.new_output {}", output.name());
        self.outputs.push(output.clone());

        let renderer = &self.renderer.as_ref().unwrap();
        let allocator = &self.allocator.as_ref().unwrap();
        output.init_render(allocator, renderer).unwrap();

        let mut state = wlroots::output::State::new();
        let mode = output.modes().next();
        if let Some(mode) = mode {
            state.set_mode(&mode);
        }
        state.set_enabled(true);
        output.commit_state(self, &state).unwrap();
    }
}

impl output::Handler for TinywlServer {
    fn destroy(&mut self, output: Output<Self>) {
        println!("output.destroy");
        self.outputs.retain(|o| o != &output);
    }

    fn frame(&mut self, output: Output<Self>) {
        let (width, height) = output.buffer_size().unwrap();
        let mut state = wlroots::output::State::new();
        let render_pass = output.begin_render_pass(&mut state).unwrap();
        render_pass.add_rect(
            &Box {
                x: 0,
                y: 0,
                width: width.try_into().unwrap(),
                height: height.try_into().unwrap(),
            },
            &Color {
                r: 1.0,
                g: 0.0,
                b: 0.0,
                a: 1.0,
            },
        );
        render_pass.submit().unwrap();
        output.commit_state(self, &state).unwrap();
    }
}

impl renderer::Handler for TinywlServer {
    fn destroy(&mut self, _: Renderer<Self>) {
        self.renderer = None;
    }
}

impl allocator::Handler for TinywlServer {
    fn destroy(&mut self, _: Allocator<Self>) {
        self.allocator = None;
    }
}

fn main() {
    log::init(log::Importance::Debug);

    let mut server = TinywlServer::default();
    let display = Display::new();
    let backend = Backend::autocreate(&mut server, &display).unwrap();
    let renderer = Renderer::autocreate(&mut server, &backend).unwrap();
    let allocator = Allocator::autocreate(&mut server, &backend, &renderer).unwrap();
    server.renderer = Some(renderer);
    server.allocator = Some(allocator);
    backend.start(&mut server).unwrap();

    display.run(&mut server);
}
