use libc::c_void;

use wlroots_sys::*;

use crate::backend::Backend;
use crate::error::Error;
use crate::listener;
use crate::object;
use crate::object::{Holder as _, Object};
use crate::renderer::Renderer;
use crate::state;

pub trait Handler: Sized {
    fn destroy(&mut self, _allocator: Allocator<Self>) {}
}

pub(crate) struct Inner<S> {
    base: object::InnerBase<Self>,
    destroy: listener::Listener,
}

impl_listener_set!(Inner, Handler, Listener, destroy);

impl<S> object::Inner for Inner<S> {
    type State = S;
    type Ptr = wlr_allocator;

    fn base(&self) -> &object::InnerBase<Self> {
        &self.base
    }

    fn destroy_ptr(&self) {
        unsafe {
            wlr_allocator_destroy(self.base.ptr());
        }
    }
}

impl<S: Handler> Listener for Inner<S> {
    fn destroy(&mut self, _: *mut c_void) {
        Object::uninit(self, |state, object| {
            Handler::destroy(state, Allocator(object.clone()));
        });
    }
}

pub struct Allocator<S>(Object<Inner<S>>);
delegate_object!(Allocator);

impl<S: Handler> Allocator<S> {
    pub fn autocreate(
        state: &mut S,
        backend: &Backend<S>,
        renderer: &Renderer<S>,
    ) -> Result<Self, Error> {
        let _guard = state::Guard::new(state);
        let wlr_backend = backend.object().ptr();
        let wlr_renderer = renderer.object().ptr();
        let ptr = unsafe { wlr_allocator_autocreate(wlr_backend, wlr_renderer).as_mut() };
        let ptr = ptr.ok_or_else(|| Error::new("wlr_allocator_autocreate failed"))?;
        Ok(Allocator(Object::new(ptr, |base| Inner {
            base,
            destroy: Default::default(),
        })))
    }
}

impl<S> object::Holder<Inner<S>> for Allocator<S> {
    fn object(&self) -> &object::Object<Inner<S>> {
        &self.0
    }
}
