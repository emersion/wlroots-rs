use libc::c_void;
use std::marker::PhantomPinned;
use std::pin::Pin;

use wayland_sys::common::wl_list;
use wayland_sys::server::signal::wl_signal_add;
use wayland_sys::server::{wl_list_remove, wl_listener, wl_notify_func_t, wl_signal};

pub(crate) struct Listener {
    pub(crate) listener: wl_listener,
    _pin: PhantomPinned,
}

extern "C" fn handle_uninit(_: *mut wl_listener, _: *mut c_void) {
    std::process::abort();
}

impl Default for Listener {
    fn default() -> Self {
        Self {
            listener: wl_listener {
                link: wl_list {
                    next: std::ptr::null_mut(),
                    prev: std::ptr::null_mut(),
                },
                notify: handle_uninit,
            },
            _pin: PhantomPinned,
        }
    }
}

impl Listener {
    pub(crate) fn init(mut self: Pin<&mut Self>, signal: *mut wl_signal, notify: wl_notify_func_t) {
        if !self.listener.link.next.is_null() {
            panic!("Listener::init() called twice");
        }
        unsafe {
            let l = self.as_mut().get_unchecked_mut();
            l.listener.notify = notify;
            wl_signal_add(signal, &mut l.listener);
        }
    }

    pub(crate) fn uninit(&mut self) {
        if self.listener.link.next.is_null() {
            panic!("Listener::uninit() called without init()");
        }
        unsafe {
            self.listener.notify = handle_uninit;
            wl_list_remove(&mut self.listener.link);
        }
    }
}

macro_rules! init_listener {
    ($ptr:expr, $container:ident, $handler:ident, $state:ty, $listener:ident, $signal:ident) => {{
        use wayland_sys::server::wl_listener;

        extern "C" fn handle<S: $handler>(l: *mut wl_listener, data: *mut c_void) {
            let ptr = unsafe {
                let ptr = container_of!(l, $container<S>, $signal.listener) as *mut $container<S>;
                ptr.as_mut().unwrap()
            };
            $listener::$signal(ptr, data);
        }

        let ptr: &mut std::pin::Pin<std::boxed::Box<$container<$state>>> = $ptr;
        let wlr_ptr = unsafe { ptr.base.ptr().as_mut().unwrap() };
        let signal = &mut wlr_ptr.events.$signal;
        let l = unsafe {
            ptr.as_mut()
                .map_unchecked_mut(|container| &mut container.$signal)
        };
        l.init(signal, handle::<$state>);
    }};
}

/// A set of [Listener].
pub(crate) trait Set {
    fn init_listeners(self: &mut Pin<Box<Self>>);
    fn uninit_listeners(&mut self);
}

/// Implement [Set] for a type.
///
/// `$container` must be a struct with:
///
/// - A `base.ptr()` function returning the raw wlroots pointer.
/// - One [Listener] field per signal, named after the signal.
/// - An implementation of the `$listener` trait with one method per signal.
macro_rules! impl_listener_set {
    ($container:ident, $handler:ident, $listener:ident, $($signal:ident),*) => {
        trait $listener {
            $(
                fn $signal(&mut self, data: *mut libc::c_void);
            )*
        }

        impl<S: $handler> $crate::listener::Set for $container<S> {
            fn init_listeners(self: &mut std::pin::Pin<std::boxed::Box<Self>>) {
                $(
                    init_listener!(self, $container, $handler, S, $listener, $signal);
                )*
            }

            fn uninit_listeners(&mut self) {
                $(
                    self.$signal.uninit();
                )*
            }
        }
    };
}
