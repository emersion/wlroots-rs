use wayland_sys::server::*;

use crate::state;

pub struct Display {
    ptr: *mut wl_display,
}

impl Display {
    pub fn new() -> Display {
        let ptr = unsafe { ffi_dispatch!(wayland_server_handle(), wl_display_create,) };
        if ptr.is_null() {
            panic!("allocation failed");
        }
        Display { ptr }
    }

    pub unsafe fn from_ptr(ptr: *mut wl_display) -> Display {
        Display { ptr }
    }

    pub const fn as_ptr(&self) -> *mut wl_display {
        self.ptr
    }

    pub fn run<S>(&self, state: &mut S) {
        let _guard = state::Guard::new(state);
        unsafe {
            ffi_dispatch!(wayland_server_handle(), wl_display_run, self.as_ptr());
        }
    }

    pub(crate) fn event_loop_ptr(&self) -> *mut wl_event_loop {
        unsafe {
            ffi_dispatch!(
                wayland_server_handle(),
                wl_display_get_event_loop,
                self.as_ptr()
            )
        }
    }
}

impl Drop for Display {
    fn drop(&mut self) {
        unsafe {
            ffi_dispatch!(wayland_server_handle(), wl_display_destroy, self.as_ptr());
        }
    }
}
