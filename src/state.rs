use std::marker::PhantomData;

static mut STATE: Option<*mut libc::c_void> = None;
static mut NREFS: u32 = 0;

pub(crate) struct Guard<S>(PhantomData<S>);

impl<S> Drop for Guard<S> {
    fn drop(&mut self) {
        unsafe {
            NREFS -= 1;
            if NREFS == 0 {
                STATE = None;
            }
        }
    }
}

impl<S> Guard<S> {
    pub(crate) fn new(state: &mut S) -> Self {
        unsafe {
            let ptr = state as *mut S as *mut libc::c_void;
            match STATE {
                Some(s) => assert!(s == ptr),
                None => STATE = Some(ptr),
            }
            NREFS += 1;
        }
        Self(PhantomData)
    }

    pub(crate) fn current<'a>() -> &'a mut S {
        unsafe { (STATE.unwrap() as *mut S).as_mut().unwrap() }
    }
}
