extern crate bindgen;
extern crate pkg_config;

use std::env;
use std::path::PathBuf;
use std::process::Command;

fn main() {
    let out_path = PathBuf::from(env::var("OUT_DIR").unwrap());

    println!("cargo:rerun-if-changed=src/wrapper.h");

    let wlroots_lib = pkg_config::Config::new()
        .probe("wlroots-0.18")
        .unwrap();

    pkg_config::Config::new()
        .probe("wayland-protocols")
        .unwrap();

    let wayland_protocols_dir = String::from_utf8(
        Command::new("pkgconf")
            .arg("wayland-protocols")
            .arg("--variable")
            .arg("pkgdatadir")
            .output()
            .unwrap()
            .stdout,
    )
    .unwrap();
    let wayland_protocols_dir = PathBuf::from(wayland_protocols_dir.trim());

    Command::new("wayland-scanner")
        .arg("server-header")
        .arg(wayland_protocols_dir.join("stable/xdg-shell/xdg-shell.xml"))
        .arg(out_path.join("xdg-shell-protocol.h"))
        .output()
        .unwrap();

    let mut builder = bindgen::builder()
        .parse_callbacks(Box::new(bindgen::CargoCallbacks::new()))
        .header("src/wrapper.h")
        .allowlist_type("wlr_.*")
        .allowlist_function("wlr_.*")
        .blocklist_type("wl_array")
        .blocklist_type("wl_client")
        .blocklist_type("wl_display")
        .blocklist_type("wl_event_loop")
        .blocklist_type("wl_event_source")
        .blocklist_type("wl_global")
        .blocklist_type("wl_list")
        .blocklist_type("wl_listener")
        .blocklist_type("wl_notify_func_t")
        .blocklist_type("wl_resource")
        .blocklist_type("wl_shm_buffer")
        .blocklist_type("wl_signal")
        .clang_arg("-DWLR_USE_UNSTABLE");

    for path_buf in wlroots_lib.include_paths {
        builder = builder.clang_arg(format!("-I{}", path_buf.to_str().unwrap()));
    }
    builder = builder.clang_arg(format!("-I{}", out_path.to_string_lossy()));

    builder
        .generate()
        .unwrap()
        .write_to_file(out_path.join("bindings.rs"))
        .unwrap();
}
