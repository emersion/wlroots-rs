use libc::{c_int, c_void};

use wlroots_sys::*;

use crate::backend::Backend;
use crate::error::Error;
use crate::listener;
use crate::object;
use crate::object::{Holder as _, Object};
use crate::state;

pub trait Handler: Sized {
    fn destroy(&mut self, _renderer: Renderer<Self>) {}
}

pub(crate) struct Inner<S> {
    base: object::InnerBase<Self>,
    destroy: listener::Listener,
}

impl_listener_set!(Inner, Handler, Listener, destroy);

impl<S> object::Inner for Inner<S> {
    type State = S;
    type Ptr = wlr_renderer;

    fn base(&self) -> &object::InnerBase<Self> {
        &self.base
    }

    fn destroy_ptr(&self) {
        unsafe {
            wlr_renderer_destroy(self.base.ptr());
        }
    }
}

impl<S: Handler> Listener for Inner<S> {
    fn destroy(&mut self, _: *mut c_void) {
        Object::uninit(self, |state, object| {
            Handler::destroy(state, Renderer(object.clone()));
        });
    }
}

pub struct Box {
    pub x: i32,
    pub y: i32,
    pub width: i32,
    pub height: i32,
}

impl Box {
    fn wlr(&self) -> wlr_box {
        wlr_box {
            x: self.x as c_int,
            y: self.y as c_int,
            width: self.width as c_int,
            height: self.height as c_int,
        }
    }
}

pub struct Color {
    pub r: f32,
    pub g: f32,
    pub b: f32,
    pub a: f32,
}

impl Color {
    fn wlr(&self) -> wlr_render_color {
        wlr_render_color {
            r: self.r,
            g: self.g,
            b: self.b,
            a: self.a,
        }
    }
}

pub struct Pass {
    ptr: *mut wlr_render_pass,
}

impl Pass {
    pub(crate) fn new(ptr: *mut wlr_render_pass) -> Pass {
        Pass { ptr }
    }

    pub fn add_rect(&self, r#box: &Box, color: &Color) {
        let options = wlr_render_rect_options {
            box_: r#box.wlr(),
            color: color.wlr(),
            clip: std::ptr::null_mut(),
            blend_mode: wlr_render_blend_mode_WLR_RENDER_BLEND_MODE_NONE,
        };
        unsafe {
            wlr_render_pass_add_rect(self.ptr, &options as *const wlr_render_rect_options);
        }
    }

    pub fn submit(self) -> Result<(), Error> {
        let ok = unsafe { wlr_render_pass_submit(self.ptr) };
        ok.then_some(())
            .ok_or_else(|| Error::new("wlr_render_pass_submit failed"))
    }
}

pub struct Renderer<S>(Object<Inner<S>>);
delegate_object!(Renderer);

impl<S: Handler> Renderer<S> {
    pub fn autocreate(state: &mut S, backend: &Backend<S>) -> Result<Self, Error> {
        let _guard = state::Guard::new(state);
        let ptr = unsafe { wlr_renderer_autocreate(backend.object().ptr()).as_mut() };
        let ptr = ptr.ok_or_else(|| Error::new("wlr_renderer_autocreate failed"))?;
        Ok(Renderer(Object::new(ptr, |base| Inner {
            base,
            destroy: Default::default(),
        })))
    }
}

impl<S> object::Holder<Inner<S>> for Renderer<S> {
    fn object(&self) -> &object::Object<Inner<S>> {
        &self.0
    }
}
