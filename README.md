# wlroots-rs

Safe Rust bindings for [wlroots].

## License

MIT

[wlroots]: https://gitlab.freedesktop.org/wlroots/wlroots
