use libc::c_void;

use wlroots_sys::*;

use crate::display::Display;
use crate::error::Error;
use crate::listener;
use crate::object;
use crate::output;
use crate::state;

pub trait Handler: Sized + output::Handler {
    fn destroy(&mut self, _backend: Backend<Self>) {}
    fn new_output(&mut self, _output: output::Output<Self>) {}
}

pub(crate) struct Inner<S> {
    base: object::InnerBase<Self>,
    destroy: listener::Listener,
    new_output: listener::Listener,
}

impl_listener_set!(Inner, Handler, Listener, destroy, new_output);

impl<S> object::Inner for Inner<S> {
    type State = S;
    type Ptr = wlr_backend;

    fn base(&self) -> &object::InnerBase<Self> {
        &self.base
    }

    fn destroy_ptr(&self) {
        unsafe {
            wlr_backend_destroy(self.base.ptr());
        }
    }
}

impl<S: Handler> Listener for Inner<S> {
    fn destroy(&mut self, _: *mut c_void) {
        object::Object::uninit(self, |state, object| {
            Handler::destroy(state, Backend(object.clone()));
        });
    }

    fn new_output(&mut self, data: *mut c_void) {
        let ptr = data as *mut wlr_output;
        let output = output::Output::new(ptr);
        state::Guard::<S>::current().new_output(output.clone());
        // TODO: need to drop Output here to avoid double borrow of state
        std::mem::drop(output);
    }
}

pub struct Backend<S>(object::Object<Inner<S>>);
delegate_object!(Backend);

impl<S: Handler> Backend<S> {
    pub fn autocreate(state: &mut S, display: &Display) -> Result<Self, Error> {
        let _guard = state::Guard::new(state);
        let ptr = unsafe {
            wlr_backend_autocreate(display.event_loop_ptr(), std::ptr::null_mut()).as_mut()
        };
        let ptr = ptr.ok_or_else(|| Error::new("wlr_backend_autocreate failed"))?;
        Ok(Backend(object::Object::new(ptr, |base| Inner {
            base,
            destroy: Default::default(),
            new_output: Default::default(),
        })))
    }

    pub fn start(&self, state: &mut S) -> Result<(), Error> {
        let _guard = state::Guard::new(state);
        let ok = unsafe { wlr_backend_start(self.0.ptr()) };
        if !ok {
            return Err(Error::new("wlr_backend_start failed"));
        }
        Ok(())
    }
}

impl<S> object::Holder<Inner<S>> for Backend<S> {
    fn object(&self) -> &object::Object<Inner<S>> {
        &self.0
    }
}
