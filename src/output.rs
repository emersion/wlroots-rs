use libc::c_void;
use std::ffi::CStr;
use std::iter::Iterator;
use std::mem::MaybeUninit;

use wlroots_sys::*;

use crate::allocator::Allocator;
use crate::error::Error;
use crate::list;
use crate::listener;
use crate::object;
use crate::object::Holder as _;
use crate::renderer;
use crate::renderer::Renderer;
use crate::state;

#[derive(Debug)]
pub struct Mode {
    pub width: i32,
    pub height: i32,
    pub refresh: i32, // mHz
    pub preferred: bool,

    wlr: *const wlr_output_mode,
}

pub struct ModeIter(list::Iter);

impl Iterator for ModeIter {
    type Item = Mode;

    fn next(&mut self) -> Option<Mode> {
        self.0.next().map(|link| {
            let ptr = unsafe {
                let ptr = container_of!(link, wlr_output_mode, link);
                ptr.as_ref().unwrap()
            };
            Mode {
                width: ptr.width,
                height: ptr.height,
                refresh: ptr.refresh,
                preferred: ptr.preferred,
                wlr: ptr,
            }
        })
    }
}

#[derive(Debug)]
pub struct State {
    wlr: wlr_output_state,
}

impl State {
    pub fn new() -> State {
        let mut wlr: MaybeUninit<wlr_output_state> = MaybeUninit::zeroed();
        let wlr = unsafe {
            wlr_output_state_init(wlr.as_mut_ptr());
            wlr.assume_init()
        };
        State { wlr }
    }

    fn ptr(&self) -> *const wlr_output_state {
        &self.wlr as *const wlr_output_state
    }

    fn mut_ptr(&mut self) -> *mut wlr_output_state {
        &mut self.wlr as *mut wlr_output_state
    }

    pub fn set_enabled(&mut self, enabled: bool) {
        unsafe {
            wlr_output_state_set_enabled(self.mut_ptr(), enabled);
        }
    }

    pub fn set_mode(&mut self, mode: &Mode) {
        unsafe {
            wlr_output_state_set_mode(self.mut_ptr(), mode.wlr as *mut wlr_output_mode);
        }
    }

    pub fn set_custom_mode(&mut self, width: i32, height: i32, refresh: Option<i32>) {
        unsafe {
            wlr_output_state_set_custom_mode(self.mut_ptr(), width, height, refresh.unwrap_or(0));
        }
    }
}

impl Drop for State {
    fn drop(&mut self) {
        unsafe {
            wlr_output_state_finish(self.mut_ptr());
        }
    }
}

pub trait Handler: Sized {
    fn destroy(&mut self, _output: Output<Self>) {}
    fn frame(&mut self, _output: Output<Self>) {}
}

struct Inner<S> {
    base: object::InnerBase<Self>,
    destroy: listener::Listener,
    frame: listener::Listener,
}

impl_listener_set!(Inner, Handler, Listener, destroy, frame);

impl<S> object::Inner for Inner<S> {
    type State = S;
    type Ptr = wlr_output;

    fn base(&self) -> &object::InnerBase<Self> {
        &self.base
    }

    fn destroy_ptr(&self) {
        unsafe {
            wlr_output_destroy(self.base.ptr());
        }
    }
}

impl<S: Handler> Listener for Inner<S> {
    fn destroy(&mut self, _: *mut c_void) {
        object::Object::uninit(self, |state, object| {
            state.destroy(Output(object.clone()));
        });
    }

    fn frame(&mut self, _: *mut c_void) {
        state::Guard::<S>::current().frame(Output(self.base.outer()));
    }
}

pub struct Output<S>(object::Object<Inner<S>>);
delegate_object!(Output);

impl<S: Handler> Output<S> {
    pub(crate) fn new(ptr: *mut wlr_output) -> Self {
        Output(object::Object::new(ptr, |base| Inner {
            base,
            destroy: Default::default(),
            frame: Default::default(),
        }))
    }

    pub fn name(&self) -> &str {
        unsafe {
            CStr::from_ptr(self.0.ptr().as_ref().unwrap().name)
                .to_str()
                .unwrap()
        }
    }

    pub fn modes(&self) -> ModeIter {
        let modes = unsafe { &mut (*self.0.ptr()).modes };
        ModeIter(list::Iter::new(modes))
    }

    pub fn buffer_size(&self) -> Option<(u32, u32)> {
        let wlr = unsafe { self.0.ptr().as_ref().unwrap() };
        if wlr.width == 0 || wlr.height == 0 {
            return None;
        }
        Some((wlr.width as u32, wlr.height as u32))
    }

    pub fn init_render(
        &self,
        allocator: &Allocator<S>,
        renderer: &Renderer<S>,
    ) -> Result<(), Error> {
        let wlr_allocator = allocator.object().ptr();
        let wlr_renderer = renderer.object().ptr();
        let ok = unsafe { wlr_output_init_render(self.0.ptr(), wlr_allocator, wlr_renderer) };
        if !ok {
            return Err(Error::new("wlr_output_init_render failed"));
        }
        Ok(())
    }

    pub fn begin_render_pass(&self, state: &mut State) -> Result<renderer::Pass, Error> {
        let pass_ptr = unsafe {
            wlr_output_begin_render_pass(
                self.0.ptr(),
                state.mut_ptr(),
                std::ptr::null_mut(),
                std::ptr::null_mut(),
            )
        };
        Ok(renderer::Pass::new(pass_ptr))
    }

    pub fn commit_state(&self, state: &mut S, output_state: &State) -> Result<(), Error> {
        let _guard = state::Guard::new(state);
        let ok = unsafe { wlr_output_commit_state(self.0.ptr(), output_state.ptr()) };
        if !ok {
            return Err(Error::new("wlr_output_commit_state failed"));
        }
        Ok(())
    }
}
