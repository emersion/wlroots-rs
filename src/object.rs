//! Object abstraction for wlroots.
//!
//! The object abstraction has two parts: a private [InnerBase] part to
//! hold common state and setup signal listeners, and a public [Object] part to
//! expose ergonomic handles to compositors.

use std::cell::Cell;
use std::pin::Pin;
use std::rc::{Rc, Weak};

use crate::listener;
use crate::state;

/// A trait which must be implemented by [Object] users.
pub(crate) trait Inner {
    /// C type for the wlroots object.
    type Ptr;
    /// State type provided by the compositor.
    type State;

    /// Get a reference to the embedded [InnerBase] for this object.
    fn base(&self) -> &InnerBase<Self>
    where
        Self: Sized;

    /// Call the wlroots destructor, if any.
    ///
    /// This will be called right before the compositor drops the last
    /// reference to the object.
    fn destroy_ptr(&self) {}
}

/// A compositor-facing handle for a wlroots object.
///
/// The handle is reference-counted so that it can be used to refer to an
/// object in event handlers.
pub(crate) struct Object<I: Inner>(Rc<Pin<Box<I>>>);

impl<I: Inner> Object<I> {
    /// Create a new object handle.
    ///
    /// The caller is responsible for initializing the inner struct with the
    /// [InnerBase]. [uninit()] must be called at the end of the destroy
    /// listener.
    pub(crate) fn new<F>(ptr: *mut I::Ptr, data_fn: F) -> Object<I>
    where
        F: FnOnce(InnerBase<I>) -> I,
        I: listener::Set,
    {
        Object(Rc::new_cyclic(|me| {
            let inner = data_fn(InnerBase {
                ptr: Cell::new(ptr),
                me: me.clone(),
            });
            let mut inner = Box::pin(inner);
            inner.init_listeners();
            inner
        }))
    }

    pub(crate) fn ptr(&self) -> *mut I::Ptr {
        self.0.base().ptr()
    }

    /// Un-initialize an object handle.
    ///
    /// This unregisters listeners, invokes the compositor callback to indicate
    /// that the object doesn't exist anymore, then unsets `ptr`.
    pub(crate) fn uninit<F>(inner: &mut I, emit_fn: F)
    where
        F: FnOnce(&mut I::State, &Self),
        I: listener::Set,
    {
        let object = inner.base().outer();
        inner.uninit_listeners();
        emit_fn(state::Guard::current(), &object);
        inner.base().ptr.set(std::ptr::null_mut());
        // Drop the last reference *after* setting the pointer to null
        std::mem::drop(object);
    }
}

impl<I: Inner> Clone for Object<I> {
    fn clone(&self) -> Self {
        Object(self.0.clone())
    }
}

impl<I: Inner> PartialEq for Object<I> {
    fn eq(&self, other: &Self) -> bool {
        Rc::ptr_eq(&self.0, &other.0)
    }
}

impl<I: Inner> Drop for Object<I> {
    fn drop(&mut self) {
        // Destroy the object right before dropping the last reference
        if Rc::strong_count(&self.0) == 1 {
            self.0.destroy_ptr();
            assert!(Rc::strong_count(&self.0) == 1);
        }
    }
}

/// A struct which must be embedded when binding a wlroots object.
pub(crate) struct InnerBase<I: Inner> {
    ptr: Cell<*mut I::Ptr>,
    me: Weak<Pin<Box<I>>>,
}

impl<I: Inner> InnerBase<I> {
    pub(crate) fn outer(&self) -> Object<I> {
        Object(self.me.upgrade().unwrap())
    }

    pub(crate) fn ptr(&self) -> *mut I::Ptr {
        self.ptr.get()
    }
}

/// Delegate [Clone] and [PartialEq] implementations for an [Object] newtype.
macro_rules! delegate_object {
    ($object:ident) => {
        impl<S> Clone for $object<S> {
            fn clone(&self) -> Self {
                $object(self.0.clone())
            }
        }

        impl<S> PartialEq for $object<S> {
            fn eq(&self, other: &Self) -> bool {
                self.0.eq(&other.0)
            }
        }
    };
}

pub(crate) trait Holder<I: Inner> {
    fn object(&self) -> &Object<I>;
}
